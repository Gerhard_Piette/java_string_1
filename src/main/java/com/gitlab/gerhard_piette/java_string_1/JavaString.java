package com.gitlab.gerhard_piette.java_string_1;

/**
 *
 */
public class JavaString {

	/**
	 * https://docs.oracle.com/javase/tutorial/java/data/characters.html
	 * <p>
	 * Note: A single \ is represented by \\\\
	 *
	 * @param str
	 * @return
	 */
	public static String addEscapeSequences(String str) {
		var ret = str;
		ret = ret.replaceAll("\\\\", "\\\\\\\\");
		ret = ret.replaceAll("\b", "\\\\b");
		ret = ret.replaceAll("\f", "\\\\f");
		ret = ret.replaceAll("\n", "\\\\n");
		ret = ret.replaceAll("\r", "\\\\r");
		ret = ret.replaceAll("\t", "\\\\t");
		ret = ret.replaceAll("\'", "\\\\\'");
		ret = ret.replaceAll("\"", "\\\\\"");
		return ret;
	}

	/**
	 * https://docs.oracle.com/javase/tutorial/java/data/characters.html
	 * <p>
	 * Note: A single \ is represented by \\\\
	 *
	 * @param str
	 * @return
	 */
	public static String removeEscapeSequences(String str) {
		var ret = str;
		ret = ret.replaceAll("\\\\b", "\b");
		ret = ret.replaceAll("\\\\f", "\f");
		ret = ret.replaceAll("\\\\n", "\n");
		ret = ret.replaceAll("\\\\r", "\r");
		ret = ret.replaceAll("\\\\t", "\t");
		ret = ret.replaceAll("\\\\\'", "\'");
		ret = ret.replaceAll("\\\\\"", "\"");
		ret = ret.replaceAll("\\\\\\\\", "\\\\");
		return ret;
	}

}
